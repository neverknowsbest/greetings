'use strict';

const fs = require('fs');
const yaml = require('js-yaml');

let $greetings = {};
const fn = __dirname + '/greetings.yml';

function randomElement(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
};

function bypass(str) {
  return (str
    .replace(/w-w/gi, match => match.split('-').join('-<A></A>'))
    .replace(/w{3,}/gi, match => match.split('').join('<A></A>'))
  );
};

function parseYml(yml) {
  try {
    $greetings = yaml.load(yml);
  } catch (e) {
    console.error(e);
  }
};

function readGreetings() {
  fs.readFile(fn, { encoding: 'utf8' }, (err, data) => {
    if (err != null) {
      console.error(err);
    } else {
      parseYml(data);
    }
  });
}

readGreetings();

fs.watch(fn, event => {
  if (event === 'change') {
    readGreetings();
  }
});

module.exports = function Greetings(dispatch) {
  const players = {};
  let name = '';
  let waiting = false;

  function sendGreet(target) {
    const greetings = $greetings[name];
    if (!greetings) return;

    const greets = greetings[target] || greetings.__default;
    if (greets && greets.length > 0) {
      dispatch.toServer('C_CHAT', 1, {
        channel: 9,
        message: bypass(randomElement(greets)),
      });
    }
  };

  dispatch.hook('S_LOGIN', dispatch.majorPatchVersion >= 114 ? 15 : 14, (event) => {
    name = event.name;
  });

  dispatch.hook('C_CHAT', 1, (event) => {
    if (event.channel === 9 && $greetings[name]) {
      return false;
    }
  });

  dispatch.hook('S_SPAWN_USER', dispatch.majorPatchVersion == 92 ? 15 : 17, (event) => {
    let id = event.gameId;
    players[id] = event.name;
  });

  dispatch.hook('S_DESPAWN_USER', 3, (event) => {
    let id = event.gameId;
    delete players[id];
  });

  dispatch.hook('C_START_INSTANCE_SKILL', dispatch.majorPatchVersion >= 114 ? 8 : 7, (event) => {
    if (event.skill.id === 60401301) { // Personalized Greetings
      const greetings = $greetings[name];
      if (!greetings) return;

      if (event.targets.length === 0) {
        sendGreet('__miss');
      } else if (event.targets.length === 1) {
        const target = event.targets[0].target;
        const id = event.targets[0].gameId;
        sendGreet(players[id]);
      } else {
        const unknown = event.targets.every(v => {
          const target = v.target;
          const id = event.targets[0].gameId;
          const user = players[id];
          return !(user && greetings[user]);
        });
        if (unknown) {
          sendGreet(null);
        } else {
          waiting = true;
        }
      }
    }
  });

  dispatch.hook('S_SYSTEM_MESSAGE', 1, (event) => {
    const msg = dispatch.parseSystemMessage(event.message);

    if (msg.id == 'SMT_FRIEND_SEND_HELLO' && waiting) {
      waiting = false;
      sendGreet(msg.tokens.UserName);
    }
  });
};
